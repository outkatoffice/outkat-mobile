import {StackNavigator, TabNavigator} from 'react-navigation'
import AddActivityScreen from '../Containers/AddActivityScreen'
import RatingScreen from '../Containers/RatingScreen'
import InterestedActivitiesScreen from '../Containers/InterestedActivitiesScreen'
import AddedActivitiesScreen from '../Containers/AddedActivitiesScreen'
import MainTabNavigationScreen from '../Containers/MainTabNavigationScreen'
import ActivityDetailsScreen from '../Containers/ActivityDetailsScreen'
import LoginScreen from '../Containers/LoginScreen'
import MyActivitiesScreen from '../Containers/MyActivitiesScreen'
import SettingsScreen from '../Containers/SettingsScreen'
import FeedScreen from '../Containers/FeedScreen'

import styles from './Styles/NavigationStyles'
import Colors from '../Themes/Colors'
import React from 'react'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  AddActivityScreen: { screen: AddActivityScreen },
  RatingScreen: { screen: RatingScreen },
  InterestedActivitiesScreen: { screen: InterestedActivitiesScreen },
  AddedActivitiesScreen: { screen: AddedActivitiesScreen },
  MainTabNavigationScreen: { screen: MainTabNavigationScreen },
  ActivityDetailsScreen: { screen: ActivityDetailsScreen },
  LoginScreen: { screen: LoginScreen },
  MyActivitiesScreen: { screen: MyActivitiesScreen },
  SettingsScreen: { screen: SettingsScreen },
  FeedScreen: { screen: FeedScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'MainTabNavigationScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav

export const MyOutkatsNavigator = TabNavigator(
  {
    AddedActivities: {
      screen: props => <AddedActivitiesScreen stackDetails={props.screenProps} />,
      navigationOptions: {
        tabBarLabel: 'ADDED'
      }
    },
    InterestedActivities: {
      screen: props => <InterestedActivitiesScreen stackDetails={props.screenProps} />,
      navigationOptions: {
        tabBarLabel: 'INTERESTS'
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: Colors.white,
      inactiveTintColor: Colors.whiteInactive,
      showIcon: false,
      style: {
        backgroundColor: Colors.accent
      },
      indicatorStyle: {
        borderBottomColor: '#000000',
        fontSize: 30
      },
      tabStyle: {
        borderRightColor: '#ffffff',
        paddingBottom: 15
      },
      labelStyle: {
        fontSize: 15,
        fontWeight: '600'
      }
    },
    initialRouteName: 'AddedActivities',
    tabBarPosition: 'top',
    animationEnabled: true,
    swipeEnabled: true
  }
)

export const FeedStack = StackNavigator(
  {
    Feed: {
      screen: FeedScreen
    },
    Rating: {
      screen: RatingScreen
    },
    Details: {
      screen: ActivityDetailsScreen
    }
  },
  {
    headerMode: 'screen'
  }
)

export const MyActivitiesStack = StackNavigator(
  {
    List: {
      screen: MyActivitiesScreen
    },
    AddActivity: {
      screen: AddActivityScreen
    },
    ActivityDetails: {
      screen: ActivityDetailsScreen
    }
  },
  {
    headerMode: 'screen'
  }
)

export const MainTabNavigator = TabNavigator(
  {
    Outkats: {
      screen: MyActivitiesStack,
      navigationOptions: {
        tabBarLabel: 'My outkats',
        tabBarIcon: ({ tintColor }) => (
          <MaterialIcons name='local-play' size={20} color={tintColor} />
        )
      }
    },
    Feed: {
      screen: FeedStack,
      navigationOptions: {
        tabBarLabel: 'Feed',
        tabBarIcon: ({ tintColor }) => (
          <MaterialIcons name='view-carousel' size={20} color={tintColor} />
        )
      }
    },
    Profile: {
      screen: SettingsScreen,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: ({ tintColor }) => (
          <MaterialIcons name='person' size={20} color={tintColor} />
        )
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: Colors.white,
      inactiveTintColor: Colors.whiteInactive,
      style: {
        backgroundColor: Colors.accent
      },
      indicatorStyle: {
        borderBottomColor: '#ffffff'
      },
      tabStyle: {
        borderRightColor: '#ffffff',
        paddingBottom: 3
      }
    },
    initialRouteName: 'Feed',
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: false
  }
)
