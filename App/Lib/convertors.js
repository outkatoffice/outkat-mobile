export const convertToDollarRating = (price) => {
  if (!price) return 0
  else if (price === 0) return 0
  else if (price < 20) return 1
  else if (price < 40) return 2
  else if (price < 60) return 3
  else if (price < 80) return 4
  return 5
}
