// a library to wrap and simplify api calls
import apisauce from 'apisauce'
import { AsyncStorage } from 'react-native'

const setJwt = async (api) => {
  const jwt = await AsyncStorage.getItem('jwt')
  console.log('JWT:', jwt)
  api.setHeader('x-auth-token', jwt)
}

// our "constructor"
const create = (baseURL = 'http://localhost:4040/api') => {
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
      'x-auth-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWYwNzFmNzE0ZDg0OWFmNGNmNWVkNDgiLCJpYXQiOjE1MjU3MDcyNjUsImV4cCI6MTUyNzAwMzI2NX0.AzJ-YvCCG6YI60z_6dXQijIZvEGxNhK8nQliwIF0N7w'
    },
    // 10 second timeout...
    timeout: 10000
  })

  const getRoot = () => api.get('')
  const getRate = () => api.get('rate_limit')
  const getUser = (username) => api.get('search/users', {q: username})

  const getActivityDetails = (activity_id) => api.get(`activity/withRatings/${activity_id}`)

  const getCurrentUser = () => api.get('user/me')

  const getRandomActivities = () => api.get(`activity/random/10`)

  const getInterestedActivities = () => api.get('user/me/interested')

  const setInterestedActivity = (activity_id) => {
    return api.post(`user/me/interested/${activity_id}`)
  }

  const setNotInterestedActivity = (activity_id) => {
    return api.post(`user/me/not_interested/${activity_id}`)
  }

  const addReview = (review) => {
    return api.post('activity/rating', review)
  }

  const setBeenThereActivity = (activity_id) =>
    api.post(`user/me/been_there/${activity_id}`)

  const addActivity = (activity) =>
    api.post(`activity/one`, activity)

  const getCreatedActivities = () => api.get('user/me/activities')

  const login = (facebook_token) => api.post('auth/facebook/', { access_token: facebook_token}).then(async (res) => {
    await AsyncStorage.setItem('jwt', res.data.token)
    console.log('jwt to storage', res.data.token)
    console.log("awaot", await getCurrentUser().then(r=>r))
    return res.data
  })

  return {
    getRoot,
    getRate,
    getUser,
    login,
    getActivityDetails,
    getCurrentUser,
    getRandomActivities,
    getInterestedActivities,
    getCreatedActivities,
    setInterestedActivity,
    setNotInterestedActivity,
    addReview,
    setBeenThereActivity,
    addActivity
  }
}

// let's return back our create method as the default.
export default api = create()
