import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/RatingScreenStyle'
import colors from '../Themes/Colors';
import View from 'react-native-ui-lib/src/components/view/index';
import {Rating} from 'react-native-ratings';
import TextInput from 'react-native-ui-lib/src/components/inputs/TextInput';
import Button from 'react-native-ui-lib/src/components/button/index';
import {activityActions} from '../modules/activity';

class RatingScreen extends Component {
  static navigationOptions = {
    title: 'Rating',
    headerStyle: {
      backgroundColor: colors.accent,
    },
    headerTintColor: colors.white,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  constructor(props) {
    super(props)
    this.state = {
      opinion: '',
      rating: 2.5
    }
  }

  componentDidMount() {
    console.log(this.props)
  }

  sendReview() {
    const review = {
      score: this.state.rating,
      description: this.state.opinion,
      activityId: this.props.navigation.state.params.activity._id
    }
    this.props.addReview(review)
    this.props.navigation.goBack()
  }

  render () {
    const activity = this.props.navigation.state.params.activity
    return (
      <View style={styles.container}>
        <View style={myStyles.myScreen}>
          <View style={myStyles.title}>
            <Text style={myStyles.titleText}>{activity.title}</Text>
          </View>
          <View style={myStyles.rating}>
            <Text style={myStyles.ratingText}>Rate the activity to help others know more about it</Text>
            <Rating
              type='star'
              ratingCount={5}
              startingValue={2.5}
              imageSize={50}
              style={myStyles.ratingComponent}
              onFinishRating={rating => this.setState({rating})}
            />
          </View>
          <View style={myStyles.textInput}>
            <TextInput
              floatingPlaceholder={false}
              floatingPlaceholderColor={colors.accent}
              placeholder='Your opinion...'
              underlineColor={colors.accent}
              onChangeText={text => this.setState({opinion: text})}
            />
          </View>
          <View>
            <Button
              label='Send Rating'
              backgroundColor={colors.accent}
              borderRadius={8}
              labelStyle={{fontWeight: 'bold'}}
              onPress={() => this.sendReview()}
            />
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {
  addReview: activityActions.addReview,
}

export default connect(mapStateToProps, mapDispatchToProps)(RatingScreen)

const myStyles = StyleSheet.create({
  myScreen: {
    margin: 20
  },
  title: {
    marginBottom: 30
  },
  titleText: {
    fontSize: 30,
    alignSelf: 'center'
  },
  rating: {
    marginBottom: 50
  },
  ratingText: {
    fontSize: 18,
    marginBottom: 30
  },
  ratingComponent: {
    alignSelf: 'center'
  },
  textInput: {
    marginBottom: 20
  }
})
