import React, {Component} from 'react'
// components
import {Image, View, ScrollView, Text} from 'react-native'
import {connect} from 'react-redux'
import Rating from 'react-native-ratings/src/rating'
import DollarRating from '../Components/DollarRating'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ActivityDetailsScreenStyle'
import { activityActions, getError, getActivityDetails, hasError, isLoading } from '../modules/activity'
import { createStructuredSelector } from 'reselect'
import colors from '../Themes/Colors';

class ActivityDetailsScreen extends Component {
  static navigationOptions = {
    title: 'Details',
    headerStyle: {
      backgroundColor: colors.accent,
    },
    headerTintColor: colors.white,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  componentDidMount () {
    this.props.getActivityDetails(this.props.navigation.state.params.activityId);
  };

  render () {
    const activityImage = this.props.activity && this.props.activity.picture || '';
    const activityDescription = this.props.activity && this.props.activity.description;
    const activityTitle = this.props.activity && this.props.activity.title;
    const activityPrice = this.props.activity && this.props.activity.price;

    console.log("DETAILS", this.props.activity)

    return (
      <ScrollView style={styles.container}>
        <View style={styles.headerContainer}>
          <Image
            source={{uri: activityImage}}
            style={styles.headerBannerImage}
          />
          <View style={styles.headerTextContainer}>
            <Text style={styles.title}>{activityTitle}</Text>
            <View style={styles.ratingContainer}>
              <Rating
                type='star'
                ratingCount={5}
                startingValue={3}
                imageSize={18}
                readonly
              />
              <View style={styles.dollarRating}>
                <DollarRating rating={activityPrice} />
              </View>
            </View>
          </View>
        </View>
        <Text style={styles.descriptionText}>{activityDescription}</Text>
      </ScrollView>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  isLoading: isLoading(),
  hasError: hasError(),
  error: getError(),
  activity: getActivityDetails()
})

const mapDispatchToProps = {
  getActivityDetails: activityActions.getActivityDetails,
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivityDetailsScreen)
