import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/AddedActivitiesScreenStyle'
import ActivityList from '../Components/ActivityList';
import View from 'react-native-ui-lib/src/components/view/index';
import {createStructuredSelector} from "reselect";
import {
  activityActions, getCreatedActivities, getError, getInterestedActivities, hasError,
  isLoading,
} from '../modules/activity';

class AddedActivitiesScreen extends Component {
  static navigationOptions = { header: null };

  componentDidMount() {
    this.props.getCreatedActivities()
  }

  render () {
    return (
      <View style={styles.listContainer} >
        <ActivityList data={this.props.created} navigation={this.props.stackDetails} listId='added' refreshFunction={this.props.getCreatedActivities} />
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  isLoading: isLoading(),
  hasError: hasError(),
  error: getError(),
  created: getCreatedActivities()
})

const mapDispatchToProps = {
  getCreatedActivities: activityActions.getCreatedActivities
}

export default connect(mapStateToProps, mapDispatchToProps)(AddedActivitiesScreen)
