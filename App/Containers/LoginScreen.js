import React, { Component } from 'react'
import { ScrollView, View, KeyboardAvoidingView } from 'react-native'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import * as FBSDK from 'react-native-fbsdk'
const { LoginButton, AccessToken } = FBSDK
import { userActions, hasError, getError, getUser, isAuthenticated, isLoading } from "../modules/user"
import styles from './Styles/LoginScreenStyle'

class LoginScreen extends Component {
  
  
  
  render () {
    const { hasError, isLoading, user, error, logout, login, isAuthenticated } = this.props
    const { navigate } = this.props.navigation
    console.log('data:', hasError, isLoading, error, user, isAuthenticated)
    if (hasError) {
      console.log('LOGIN ERROR:', error)
    }
    
    if (isAuthenticated) {
      console.log("AUTHENTICATED-GO TO MAIN SCREEN")
      // navigate('LaunchScreen')
    }
    
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <View>
            <LoginButton
              publishPermissions={["publish_actions"]}
              onLoginFinished={(error, result) => {
                if (error) {
                  alert("login has error: " + result.error);
                } else if (result.isCancelled) {
                  alert("login is cancelled.");
                } else {
                  AccessToken.getCurrentAccessToken().then(
                    (data) => {
                      const token = data.accessToken.toString()
                      console.log("ACCESStoKEN:", token)
                      if (token) {
                        login(token)
                      }
                    }
                  )
                }
              }}
              onLogoutFinished={() => logout()}
            />
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  isLoading: isLoading(),
  hasError: hasError(),
  error: getError(),
  user: getUser(),
  isAuthenticated: isAuthenticated()
})

const mapDispatchToProps = {
  login: userActions.login,
  loginFailure: userActions.loginFailure,
  logout: userActions.logout
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
