import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  listContainer: {
    flex: 1,
    paddingTop: 10
  }
})
