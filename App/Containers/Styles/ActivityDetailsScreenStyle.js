import {Dimensions, StyleSheet} from 'react-native'

const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  headerBannerImage: {
    width: width,
    height: height * 0.3
  },
  headerTextContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    width: width,
    height: height * 0.45,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: 0.2,
    borderRadius: 6
  },
  ratingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 0,
    paddingLeft: 10,
    paddingRight: 10
  },
  title: {
    fontSize: 25,
    fontWeight: '600',
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10
  },
  descriptionText: {
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10,
    fontSize: 15
  }
})
