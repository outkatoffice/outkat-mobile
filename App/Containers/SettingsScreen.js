import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/SettingsScreenStyle'
import Profile from '../Components/Profile';
import View from 'react-native-ui-lib/src/components/view/index';
import { createStructuredSelector } from 'reselect'
import {getError, getUser, hasError, isLoading, userActions} from '../modules/user';

class SettingsScreen extends Component {
  componentDidMount() {
    const { getCurrentUser } = this.props
    getCurrentUser()
  }

  render () {
    const { currentUser, isLoading, error, hasError } = this.props
    return (
      <View style={styles.container}>
        {
          currentUser ? <Profile user={currentUser} /> : null
        }
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  isLoading: isLoading(),
  hasError: hasError(),
  error: getError(),
  currentUser: getUser()
})

const mapDispatchToProps = {
  getCurrentUser: userActions.getCurrentUser
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen)
