import React, { Component } from 'react'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/MyActivitiesScreenStyle'
import {MyOutkatsNavigator} from '../Navigation/AppNavigation';
import View from 'react-native-ui-lib/src/components/view/index';
import TouchableOpacity from 'react-native-ui-lib/src/components/touchableOpacity/index';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import colors from '../Themes/Colors';

const buttonStyle = {
  roundButtonContainer: {
    borderWidth: 0,
    borderColor: 'rgba(0,0,0,0.1)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 65,
    height: 65,
    backgroundColor: '#F1636A',
    borderRadius: 100,
    alignSelf: 'flex-end',
    shadowColor: '#888888',
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    position: 'absolute',
    bottom: 0,
    right: 0
  },
}

class MyActivitiesScreen extends Component {
  static navigationOptions = { header: null };

  handleAddActivity() {
    this.props.navigation.navigate('AddActivity')
  }

  render () {
    return (
      <View style={styles.container}>
        <MyOutkatsNavigator screenProps={this.props.navigation} style={{ flex: 1 }} />
        <View style={{margin: 20}}>
          <TouchableOpacity style={buttonStyle.roundButtonContainer} onPress={() => {this.handleAddActivity()}}>
            <MaterialIcons name='add' size={30} style={{color: colors.white}}/>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyActivitiesScreen)
