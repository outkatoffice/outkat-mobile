import React, { Component } from 'react'
import { connect } from 'react-redux'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/MainTabNavigationScreenStyle'
import View from 'react-native-ui-lib/src/components/view/index';
import {MainTabNavigator} from '../Navigation/AppNavigation';
import {SafeAreaView, StatusBar} from 'react-native';
import Colors from '../Themes/Colors';
import * as AsyncStorage from 'react-native/Libraries/Storage/AsyncStorage';

class MainTabNavigationScreen extends Component {
  componentDidMount() {
    AsyncStorage.clear();
  //  SO YOU DO NOT GET WEIRD CACHING ISSUES
  }

  render () {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.container}>
          <StatusBar
            backgroundColor={Colors.accent}
            barStyle='dark-content'
          />
          <MainTabNavigator />
        </View>
      </SafeAreaView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainTabNavigationScreen)
