import React, { Component } from 'react'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/FeedScreenStyle'
import View from 'react-native-ui-lib/src/components/view/index';
import SwipeCards from '../Components/SwipeCards';
import {createStructuredSelector} from "reselect";
import {getError, getFeedActivities, hasError, isLoading, activityActions} from '../modules/activity';

class FeedScreen extends Component {
  static navigationOptions = { title: 'Feed', header: null };

  componentDidMount() {
    this.props.getRandomActivities()
  }

  render () {
    return (
      <View style={styles.container}>
        {
          this.props.activities.length > 0 ?
            <SwipeCards cards={this.props.activities} navigation={this.props.navigation} /> :
            null
        }
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  isLoading: isLoading(),
  hasError: hasError(),
  error: getError(),
  activities: getFeedActivities()
})

const mapDispatchToProps = {
  getRandomActivities: activityActions.getRandomActivities
}

export default connect(mapStateToProps, mapDispatchToProps)(FeedScreen)
