import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/AddActivityScreenStyle'
import colors from '../Themes/Colors';
import AddActivity from '../Components/AddActivity';

class AddActivityScreen extends Component {
  static navigationOptions = {
    title: 'Add Activity',
    headerStyle: {
      backgroundColor: colors.accent,
    },
    headerTintColor: colors.white,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <AddActivity navigation={this.props.navigation}/>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddActivityScreen)
