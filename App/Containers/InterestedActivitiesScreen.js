import React, { Component } from 'react'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/InterestedActivitiesScreenStyle'
import ActivityList from '../Components/ActivityList';
import View from 'react-native-ui-lib/src/components/view/index';
import {createStructuredSelector} from "reselect";
import {
  activityActions, getError, getFeedActivities, getInterestedActivities, hasError,
  isLoading,
} from '../modules/activity';

class InterestedActivitiesScreen extends Component {
  static navigationOptions = { header: null };

  componentDidMount() {
    this.props.getInterestedActivities()
  }

  render () {
    return (
      <View style={styles.listContainer} >
        <ActivityList data={this.props.interested} navigation={this.props.stackDetails} listId='interested' refreshFunction={this.props.getInterestedActivities} />
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  isLoading: isLoading(),
  hasError: hasError(),
  error: getError(),
  interested: getInterestedActivities()
})

const mapDispatchToProps = {
  getInterestedActivities: activityActions.getInterestedActivities
}

export default connect(mapStateToProps, mapDispatchToProps)(InterestedActivitiesScreen)
