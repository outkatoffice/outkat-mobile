import { userSagas } from '../modules/user'
import { activitySagas } from '../modules/activity'

export default function * root () {
  yield [
    ...userSagas,
    ...activitySagas
  ]
}
