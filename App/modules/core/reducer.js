/* ------------- Initial State ------------- */
import Immutable from 'seamless-immutable';

export const INITIAL_STATE = Immutable({
  locationFilter: 50,
  priceFilter: 3
})

export const setPriceFilter = (state, action) => {
  const priceFilter = action.priceFilter
  return state.merge({ priceFilter })
}

export const setLocationFilter = (state, action) => {
  const locationFilter = action.locationFilter
  return state.merge({ locationFilter })
}

/* ------------- Hookup Reducers To Types ------------- */
import {coreTypes as Types} from '../core/actions';
import {createReducer} from 'reduxsauce';

export const coreReducer = createReducer(INITIAL_STATE, {
  [Types.SET_LOCATION_FILTER]: setLocationFilter,
  [Types.SET_PRICE_FILTER]: setPriceFilter
})
