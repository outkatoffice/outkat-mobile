import { createSelector } from 'reselect'

const getBase = () => state => state.core

const getLocationFilter = () => createSelector(
  getBase(),
  core => core.locationFilter
)

const getPriceFilter = () => createSelector(
  getBase(),
  core => core.priceFilter
)

export {
  getLocationFilter,
  getPriceFilter
}

