import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  setLocationFilter: ['locationFilter'],
  setPriceFilter: ['priceFilter']
})

const coreTypes = Types
const coreActions = Creators
export { coreTypes, coreActions }
