import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  login: ['facebook_token'],
  loginSuccess: ['user'],
  loginFailure: ['error'],
  logout: null,
  logoutSuccess: null,
  logoutFailure: ['error'],
  getCurrentUser: null,
  getCurrentUserSuccess: ['user'],
  getCurrentUserFailure: ['error']
})

const userTypes = Types
const userActions = Creators
export { userTypes, userActions }
