import { createSelector } from 'reselect'
const getBase = () => state => state.user

const isAuthenticated = () => createSelector(
  getBase(),
  user => user.authenticated
)

const isLoading = () => createSelector(
  getBase(),
  user => user.isLoading
)

const getUser = () => createSelector(
  getBase(),
  user => user.user
)

const hasError = () => createSelector(
  getBase(),
  user => (user.error !== undefined && user.error !== null)
)

const getError = () => createSelector(
  getBase(),
  user => user.error
)

export {
  isAuthenticated,
  isLoading,
  getUser,
  getError,
  hasError
}

