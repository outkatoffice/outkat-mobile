import { createReducer } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { userTypes as Types } from "./actions"

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  isLoading: false,
  authenticated: false,
  user: null,
  error: null
})

/* ------------- Reducers ------------- */
export const login = (state) =>
  state.merge({ isLoading: true, authenticated: false })

export const loginSuccess = (state, action) => {
  const { user } = action
  return state.merge({ isLoading: false, error: null, user, authenticated: user !== undefined && user !== null })
}

export const loginFailure = (state, { error }) =>
  state.merge({ isLoading: false, error, user: null, authenticated: false })

export const logout = (state, {  }) =>
  state.merge({ isLoading: false, authenticated: false, user: null, error: null })

export const logoutSuccess = (state) => {
  return state.merge({ isLoading: false, error: null, user: null, authenticated: false })
}

export const logoutFailure = (state, { error }) =>
  state.merge({ isLoading: false, error, authenticated: false })

export const getCurrentUser = (state) =>
  state.merge({ isLoading: true })

export const getCurrentUserSuccess = (state, action) => {
  const { user } = action
  return state.merge({ isLoading: false, error: null, user })
}

export const getCurrentUserFailure = (state, { error }) =>
  state.merge({ isLoading: false, error, user: null })


/* ------------- Hookup Reducers To Types ------------- */
export const userReducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN]: login,
  [Types.LOGIN_SUCCESS]: loginSuccess,
  [Types.LOGIN_FAILURE]: loginFailure,
  [Types.LOGOUT]: logout,
  [Types.LOGOUT_SUCCESS]: logoutSuccess,
  [Types.LOGOUT_FAILURE]: logoutFailure,
  [Types.GET_CURRENT_USER]: getCurrentUser,
  [Types.GET_CURRENT_USER_SUCCESS]: getCurrentUserSuccess,
  [Types.GET_CURRENT_USER_FAILURE]: getCurrentUserFailure,
})
