import { call, put, fork, take } from 'redux-saga/effects'
import { userTypes, userActions } from './actions'
import api from '../../Services/Api'

// ----------------------------------------------------------------- LOGIN SAGA
function * login (token) {
  try {

    const response = yield call(api.login, token)
    console.log("LOGIN:", response.data)
    // const usr = yield call(api.getCurrentUser, token)
    // console.log("USERR::", usr)
    yield put(userActions.loginSuccess(response.data.user))
  } catch (err) {
    console.log("LOGIN SAGAS ERROR:", err)
    yield put(userActions.loginFailure(err))
  }
}

function * watchLogin () {
  while (true) {
    const payload = yield take(userTypes.LOGIN)
    yield fork(login, payload.facebook_token)
  }
}

// ----------------------------------------------------------------- GET CURRENT USER SAGA
function * getCurrentUser () {
  try {
    const response = yield call(api.getCurrentUser)
    console.log("CURRENT USER: ", response.data)
    yield put(userActions.getCurrentUserSuccess(response.data))
  } catch (err) {
    console.log("GET CURRENT USER SAGAS FAILURE", err)
    yield put(userActions.getCurrentUserFailure(err))
  }
}

function * watchGetCurrentUser () {
  while (true) {
    const payload = yield take(userTypes.GET_CURRENT_USER)
    console.log('PAYLOAD ', payload)
    yield fork(getCurrentUser)
  }
}

export const userSagas = [
  fork(watchLogin),
  fork(watchGetCurrentUser)
]
