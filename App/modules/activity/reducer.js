import { createReducer } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { activityTypes as Types } from "./actions"

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  feed: [],
  interested: [],
  created: [],
  isLoading: false,
  error: null,
  activityDetails: null
})

/* ------------- Reducers ------------- */
export const getRandomActivities = (state) => {
  return state.merge({ isLoading: true })
}

export const getRandomActivitiesSuccess = (state, action) => {
  const feed = action.activities
  return state.merge({ isLoading: false, error: null, feed })
}

export const getRandomActivitiesFailure = (state, { error }) =>
  state.merge({ isLoading: false, error, feed: null })

export const getInterestedActivities = (state) =>
  state.merge({ isLoading: true })

export const getInterestedActivitiesSuccess = (state, action) => {
  const interested = action.activities
  return state.merge({ isLoading: false, error: null, interested })
}

export const getInterestedActivitiesFailure = (state, {error}) =>
  state.merge({ isLoading: false, error, interested: [] })

export const getCreatedActivities = (state) =>
  state.merge({ isLoading: true })

export const getCreatedActivitiesSuccess = (state, action) => {
  const created = action.activities
  return state.merge({ isLoading: false, error: null, created })
}

export const getCreatedActivitiesFailure = (state, {error}) =>
  state.merge({ isLoading: false, error, created: [] })

export const getActivityDetails = (state) =>
  state.merge({isLoading: true})

export const getActivityDetailsSuccess = (state, action) => {
  const activityDetails = action.activity
  return state.merge({isLoading: false, error: null, activityDetails})
}

export const getActivityDetailsFailure = (state, {error}) => {
  state.merge({isLoading: false, error, activityDetails: null})
}

//REDUCERS FOR SWIPING INTERACTIONS

export const setInterestedActivity = (state) =>
  state.merge({ isLoading: true })

export const setInterestedActivitySuccess = (state, action) =>
  state.merge({ isLoading: false, error: null })

export const setInterestedActivityFailure = (state, { err }) =>
  state.merge({ isLoading: false, error: err })

export const setNotInterestedActivity = (state) =>
  state.merge({ isLoading: true })

export const setNotInterestedActivitySuccess = (state, action) =>
  state.merge({ isLoading: false, error: null })

export const setNotInterestedActivityFailure = (state, { err }) =>
  state.merge({ isLoading: false, error: err })

export const addReview = (state) =>
  state.merge({ isLoading: true })

export const addReviewSuccess = (state, action) =>
  state.merge({ isLoading: false, error: null })

export const addReviewFailure = (state, { err }) =>
  state.merge({ isLoading: false, error: err })

export const setBeenThereActivity = (state) =>
  state.merge({ isLoading: true })

export const setBeenThereActivitySuccess = (state, action) =>
  state.merge({ isLoading: false, error: null })

export const setBeenThereActivityFailure = (state, { err }) =>
  state.merge({ isLoading: false, error: err })

export const addActivity = (state) =>
  state.merge({ isLoading: true })

export const addActivitySuccess = (state, action) =>
  state.merge({ isLoading: false, error: null })

export const addActivityFailure = (state, { err }) =>
  state.merge({ isLoading: false, error: err })

/* ------------- Hookup Reducers To Types ------------- */
export const activityReducer = createReducer(INITIAL_STATE, {
  [Types.GET_RANDOM_ACTIVITIES]: getRandomActivities,
  [Types.GET_RANDOM_ACTIVITIES_SUCCESS]: getRandomActivitiesSuccess,
  [Types.GET_RANDOM_ACTIVITIES_FAILURE]: getRandomActivitiesFailure,
  [Types.GET_INTERESTED_ACTIVITIES]: getInterestedActivities,
  [Types.GET_INTERESTED_ACTIVITIES_SUCCESS]: getInterestedActivitiesSuccess,
  [Types.GET_INTERESTED_ACTIVITIES_FAILURE]: getInterestedActivitiesFailure,
  [Types.GET_CREATED_ACTIVITIES]: getCreatedActivities,
  [Types.GET_CREATED_ACTIVITIES_SUCCESS]: getCreatedActivitiesSuccess,
  [Types.GET_CREATED_ACTIVITIES_FAILURE]: getCreatedActivitiesFailure,
  [Types.GET_ACTIVITY_DETAILS]: getActivityDetails,
  [Types.GET_ACTIVITY_DETAILS_SUCCESS]: getActivityDetailsSuccess,
  [Types.GET_ACTIVITY_DETAILS_FAILURE]: getActivityDetailsFailure,
  [Types.SET_INTERESTED_ACTIVITY]: setInterestedActivity,
  [Types.SET_INTERESTED_ACTIVITY_SUCCESS]: setInterestedActivitySuccess,
  [Types.SET_INTERESTED_ACTIVITY_FAILURE]: setInterestedActivityFailure,
  [Types.SET_NOT_INTERESTED_ACTIVITY]: setNotInterestedActivity,
  [Types.SET_NOT_INTERESTED_ACTIVITY_SUCCESS]: setNotInterestedActivitySuccess,
  [Types.SET_NOT_INTERESTED_ACTIVITY_FAILURE]: setNotInterestedActivityFailure,
  [Types.ADD_REVIEW]: addReview,
  [Types.ADD_REVIEW_SUCCESS]: addReviewSuccess,
  [Types.ADD_REVIEW_FAILURE]: addReviewFailure,
  [Types.SET_BEEN_THERE_ACTIVITY]: setBeenThereActivity,
  [Types.SET_BEEN_THERE_ACTIVITY_SUCCESS]: setBeenThereActivitySuccess,
  [Types.SET_BEEN_THERE_ACTIVITY_FAILURE]: setBeenThereActivityFailure,
  [Types.ADD_ACTIVITY]: addActivity,
  [Types.ADD_ACTIVITY_SUCCESS]: addActivitySuccess,
  [Types.ADD_ACTIVITY_FAILURE]: addActivityFailure,
})
