// ----------------------------------------------------------------- GET CURRENT USER SAGA
import {call, fork, put, take} from 'redux-saga/effects';
import api from '../../Services/Api';
import {activityActions, activityTypes} from '../activity/actions';

// GET RANDOM ACTIVITIES

const RANDOM_ACTIVITIES_COUNT = 10

function * getRandomActivities () {
  try {
    const response = yield call(api.getRandomActivities)
    yield put(activityActions.getRandomActivitiesSuccess(response.data))
  } catch (err) {
    yield put(activityActions.getRandomActivitiesFailure(err))
  }
}

function * watchGetRandomActivities() {
  while(true) {
    const payload = yield take(activityTypes.GET_RANDOM_ACTIVITIES)
    yield fork(getRandomActivities)
  }
}

// GET INTERESTED ACTIVITIES

function * getInterestedActivities () {
  try {
    const response = yield call(api.getInterestedActivities)
    yield put(activityActions.getInterestedActivitiesSuccess(response.data))
  } catch (err) {
    yield put(activityActions.getInterestedActivitiesFailure(err))
  }
}

function * watchGetInterestedActivities() {
  while(true) {
    const payload = yield take(activityTypes.GET_INTERESTED_ACTIVITIES)
    yield fork(getInterestedActivities)
  }
}

// GET MY CREATED ACTIVITIES

function * getCreatedActivities () {
  try {
    const response = yield call(api.getCreatedActivities)
    yield put(activityActions.getCreatedActivitiesSuccess(response.data.createdActivities))
  } catch (err) {
    yield put(activityActions.getCreatedActivitiesFailure(err))
  }
}

function * watchGetCreatedActivities() {
  while(true) {
    const payload = yield take(activityTypes.GET_CREATED_ACTIVITIES)
    yield fork(getCreatedActivities)
  }
}

// GET ACTIVITY DETAILS

function * getActivityDetails (activity_id) {
  try {
    const response = yield call(api.getActivityDetails, activity_id)
    yield put(activityActions.getActivityDetailsSuccess(response.data))
  } catch (err) {
    yield put(activityActions.getActivityDetailsFailure(err))
  }
}

function * watchGetActivityDetails() {
  while (true) {
    const payload = yield take(activityTypes.GET_ACTIVITY_DETAILS)
    yield fork(getActivityDetails, payload.activity_id)
  }
}

// SET ACTIVITY AS INTERESTED

function * setInterestedActivity (activity_id) {
  try {
    const response = yield call(api.setInterestedActivity, activity_id)
    yield put(activityActions.setInterestedActivitySuccess(response.data))
  } catch (err) {
    yield put(activityActions.setInterestedActivityFailure(err))
  }
}

function * watchSetInterestedActivity() {
  while(true) {
    const payload = yield take(activityTypes.SET_INTERESTED_ACTIVITY)
    yield fork(setInterestedActivity, payload.activity_id)
  }
}

// SET ACTIVITY AS NOT INTERESTED

function * setNotInterestedActivity (activity_id) {
  try {
    const response = yield call(api.setNotInterestedActivity, activity_id)
    yield put(activityActions.setNotInterestedActivitySuccess(response.data))
  } catch (err) {
    yield put(activityActions.setNotInterestedActivityFailure(err))
  }
}

function * watchSetNotInterestedActivity() {
  while(true) {
    const payload = yield take(activityTypes.SET_NOT_INTERESTED_ACTIVITY)
    yield fork(setNotInterestedActivity, payload.activity_id)
  }
}

// ADD RATING FOR ACTIVITY

function * addReview (review) {
  try {
    const response = yield call(api.addReview, review)
    yield put(activityActions.addReviewSuccess(response.data))
  } catch (err) {
    yield put(activityActions.addReviewFailure(err))
  }
}

function * watchAddReview() {
  while(true) {
    const payload = yield take(activityTypes.ADD_REVIEW)
    yield fork(addReview, payload.review)
  }
}

// SET ACTIVITY AS NOT INTERESTED

function * setBeenThereActivity (activity_id) {
  try {
    const response = yield call(api.setBeenThereActivity, activity_id)
    yield put(activityActions.setBeenThereActivitySuccess(response.data))
  } catch (err) {
    yield put(activityActions.setBeenThereActivityFailure(err))
  }
}

function * watchSetBeenThereActivity() {
  while(true) {
    const payload = yield take(activityTypes.SET_BEEN_THERE_ACTIVITY)
    yield fork(setBeenThereActivity, payload.activity_id)
  }
}

// ADD ACTIVITY

function * addActivity (activity) {
  try {
    const response = yield call(api.addActivity, activity)
    yield put(activityActions.addActivitySuccess(response.data))
  } catch (err) {
    yield put(activityActions.addActivityFailure(err))
  }
}

function * watchAddActivity() {
  while(true) {
    const payload = yield take(activityTypes.ADD_ACTIVITY)
    yield fork(addActivity, payload.activity)
  }
}

export const activitySagas = [
  fork(watchGetRandomActivities),
  fork(watchGetInterestedActivities),
  fork(watchGetCreatedActivities),
  fork(watchSetInterestedActivity),
  fork(watchSetNotInterestedActivity),
  fork(watchAddReview),
  fork(watchSetBeenThereActivity),
  fork(watchGetActivityDetails),
  fork(watchSetBeenThereActivity),
  fork(watchAddActivity)
]
