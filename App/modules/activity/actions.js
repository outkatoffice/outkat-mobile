import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  getRandomActivities: null,
  getRandomActivitiesSuccess: ['activities'],
  getRandomActivitiesFailure: ['error'],
  getInterestedActivities: null,
  getInterestedActivitiesSuccess: ['activities'],
  getInterestedActivitiesFailure: ['error'],
  getCreatedActivities: null,
  getCreatedActivitiesSuccess: ['activities'],
  getCreatedActivitiesFailure: ['error'],
  getActivityDetails: ['activity_id'],
  getActivityDetailsSuccess: ['activity'],
  getActivityDetailsFailure: ['error'],
  setInterestedActivity: ['activity_id'],
  setInterestedActivitySuccess: ['activity'],
  setInterestedActivityFailure: ['error'],
  setNotInterestedActivity: ['activity_id'],
  setNotInterestedActivitySuccess: ['activity'],
  setNotInterestedActivityFailure: ['error'],
  addReview: ['review'],
  addReviewSuccess: ['review'],
  addReviewFailure: ['error'],
  setBeenThereActivity: ['activity_id'],
  setBeenThereActivitySuccess: ['activity'],
  setBeenThereActivityFailure: ['error'],
  addActivity: ['activity'],
  addActivitySuccess: ['activity'],
  addActivityFailure: ['error'],
})

const activityTypes = Types
const activityActions = Creators
export { activityTypes, activityActions}
