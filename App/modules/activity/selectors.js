import {createSelector} from 'reselect/src/index';

const getBase = () => state => {
  return state.activity
}

const getFeedActivities = () => createSelector(
  getBase(),
  activity => activity.feed
)

const getInterestedActivities = () => createSelector(
  getBase(),
  activity => {
    return activity.interested
  }
)

const getCreatedActivities = () => createSelector(
  getBase(),
  activity => {
    return activity.created
  }
)

const getActivityDetails = () => createSelector(
  getBase(),
  activity => activity.activityDetails
)

const isLoading = () => createSelector(
  getBase(),
  activity => activity.isLoading
)

const hasError = () => createSelector(
  getBase(),
  activity => (activity.error !== undefined && activity.error !== null)
)

const getError = () => createSelector(
  getBase(),
  activity => activity.error
)

export {
  getActivityDetails,
  getFeedActivities,
  getInterestedActivities,
  getCreatedActivities,
  isLoading,
  hasError,
  getError
}
