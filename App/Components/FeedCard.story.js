import React from 'react'
import { storiesOf } from '@storybook/react-native'

import FeedCard from './FeedCard'
import {View} from 'react-native';

storiesOf('Feed')
  .add('FeedCard', () => (
    <View style={{flex: 1}}>
      <FeedCard title="Cool title!" dollarRating={4} starRating={4} />
    </View>
  ))
