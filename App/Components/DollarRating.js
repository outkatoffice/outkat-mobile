import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View } from 'react-native'
import styles from './Styles/DollarRatingStyle'
// import Icon from 'react-native-vector-icons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class DollarRating extends Component {
  render () {
    const size = this.props.size ? this.props.size : 20
    const containerStyle = this.props.containerStyle ? this.props.containerStyle : styles.container
    let {rating} = this.props
    if (rating > 5) {
      rating = 5
    }
    return (
      <View style={containerStyle} >
        {[...Array(rating)].map((x, idx) =>
          <MaterialIcons key={`dollar${idx}`} name="attach-money" size={size} style={styles.starRating} />
        )}
      </View>
    )
  }
}
