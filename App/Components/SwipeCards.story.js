import React from 'react'
import { storiesOf } from '@storybook/react-native'

import View from 'react-native-ui-lib/src/components/view/index'
import SwipeCards from './SwipeCards';

const myCards = [
  {title: 'Card1', dollarRating: 4, starRating: 3},
  {title: 'Card2', dollarRating: 1, starRating: 2},
  {title: 'Card3', dollarRating: 3, starRating: 3.8},
  {title: 'Card4', dollarRating: 3, starRating: 5},
  {title: 'Card5', dollarRating: 5, starRating: 5}
]

storiesOf('Feed')
  .add('SwipeCards', () => (
    <View style={{flex: 1}}>
      <SwipeCards cards={myCards} />
    </View>
  ))
