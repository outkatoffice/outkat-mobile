import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, ScrollView } from 'react-native'
import styles from './Styles/AddActivityStyle'
import TextInput from 'react-native-ui-lib/src/components/inputs/TextInput';
import colors from '../Themes/Colors';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TouchableOpacity from 'react-native-ui-lib/src/components/touchableOpacity/index';
import Stepper from 'react-native-ui-lib/src/components/stepper/index';
import TagsInput from 'react-native-ui-lib/src/components/tagsInput/index';
import Button from 'react-native-ui-lib/src/components/button/index';

import {connect} from 'react-redux';
import {createStructuredSelector} from "reselect";
import {getError, hasError, isLoading, activityActions} from '../modules/activity';



import ImagePicker from 'react-native-image-crop-picker';
import Image from 'react-native-ui-lib/src/components/image/index';

class AddActivity extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
      description: '',
      location: [],
      geoSearchText: '',
      price: 1,
      tags: [],
      image_base64: '',
      image_uri: ''
    }
  }

  handleGetCurrentLocation() {
    navigator.geolocation.getCurrentPosition(
      ({ coords }) => {
        const location = [coords.latitude, coords.longitude]
        const geoSearchText = `${location[0]}, ${location[1]}`
        this.setState({location, geoSearchText})
      }
    );
  }

  handleAddActivity() {
    const activity = {
      title: this.state.title,
      description: this.state.description,
      tags: this.state.tags.map(t => t.label),
      location: this.state.location,
      image: this.state.image_base64,
      price: this.state.price
    }
    this.props.addActivity(activity)
    this.props.navigation.goBack()
  }

  handleSelectGeo(details, data) {
    const location = [details.geometry.location.lat, details.geometry.location.lng]
    const geoSearchText = data.description
    this.setState({location, geoSearchText})
  }

  handleSelectImage() {
    ImagePicker.openPicker({
      cropping: true,
      includeBase64: true,
      compressImageMaxWidth: 800,
      compressImageMaxHeight: 800,
      compressImageQuality: 0.5,
      mediaType: 'photo'
    }).then(image => {
      this.setState({image_uri: image.sourceURL, image_base64: image.data})
    });
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <TouchableOpacity style={styles.imageContainer} onPress={() => {this.handleSelectImage()}}>
          <Image source={{uri: this.state.image_uri}} style={styles.coverImage} />
          {
            !this.state.image_uri ?
              <Text style={styles.textImageOverlay}>Upload Image</Text> :
              null
          }
        </TouchableOpacity>
        <View>
          <TextInput
            floatingPlaceholder={false}
            floatingPlaceholderColor={colors.accent}
            placeholder='Title'
            underlineColor={colors.accent}
            onChangeText={title => this.setState({title})}
          />
          <TextInput
            floatingPlaceholder={false}
            floatingPlaceholderColor={colors.accent}
            placeholder='Description'
            underlineColor={colors.accent}
            onChangeText={description => this.setState({description})}
          />
        </View>
        <View>
          <GooglePlacesAutocomplete
            placeholder='Location'
            minLength={2}
            autoFocus={false}
            returnKeyType={'search'}
            listViewDisplayed='auto'
            fetchDetails={true}
            renderDescription={row => row.description} // custom description render
            onPress={(data, details = null) => {this.handleSelectGeo(details, data)}}
            text={this.state.geoSearchText}
            getDefaultValue={() => ''}
            query={{
              // available options: https://developers.google.com/places/web-service/autocomplete
              key: 'AIzaSyBcxzWOemFXZXJ-iYIu9FhhcVkP0FGqsEg',
              language: 'en', // language of the results
              types: 'geocode' // default: 'geocode'
            }}
            styles={{
              textInputContainer: styles.geoTextInputContainer,
              textInput: styles.geoTextInput,
              description: styles.geoDescription,
              predefinedPlacesDescription: styles.predefinedPlaces
            }}
            currentLocation={false}
            nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
            GooglePlacesSearchQuery={{
              rankby: 'distance',
              types: 'food'
            }}
            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
            debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
            renderRightButton={() =>
              <TouchableOpacity onPress={() => {this.handleGetCurrentLocation()}}>
                <MaterialIcons name='my-location' size={25} color={colors.accent} style={styles.currentLocationIcon}/>
              </TouchableOpacity>
            }
          />
        </View>
        <View style={styles.stepperContainer}>
          <View style={styles.stepperTextContainer}>
            <Text style={styles.stepperText}>Price: {"$".repeat(this.state.price)}</Text>
          </View>
          <View>
            <Stepper initialValue={1} min={0} max={5} onValueChange={(price) => {this.setState({price})}}/>
          </View>
        </View>
        <TagsInput
          containerStyle={{marginBottom: 20}}
          placeholder="Tags"
          tags={this.state.tags}
          onCreateTag={value => ({label: value})}
          onChangeTags={tags => this.setState({tags})}
          inputStyle={styles.tagsInputStyle}
          tagStyle={styles.tagsTagStyle}
        />
        <Button label='Add' borderRadius={5} backgroundColor={colors.accent} onPress={() => {this.handleAddActivity()}}/>
      </ScrollView>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  isLoading: isLoading(),
  hasError: hasError(),
  error: getError(),
})

const mapDispatchToProps = {
  addActivity: activityActions.addActivity
}

export default connect(mapStateToProps, mapDispatchToProps)(AddActivity)
