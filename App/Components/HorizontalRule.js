import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/HorizontalRuleStyle'

export default class HorizontalRule extends Component {
  render () {
    return (
      <View
        style={{
          borderBottomColor: 'lightgrey',
          borderBottomWidth: 1,
          marginBottom: 25
        }}
      />
    )
  }
}
