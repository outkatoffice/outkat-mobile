import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import {View, Text, FlatList, RefreshControl} from 'react-native'
import styles from './Styles/ActivityListStyle'
import ActivityListItem from './ActivityListItem';

export default class ActivityList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      refreshing: false
    }
  }

  _keyExtractor = (item) => item._id + this.props.listId;

  _handleRefresh() {
    if (this.props.refreshFunction) {
      this.props.refreshFunction()
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.data}
          keyExtractor={this._keyExtractor}
          renderItem={({item}) => <ActivityListItem navigation={this.props.navigation} key={item._id + this.props.listId} activity={item} />}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {this._handleRefresh()}}
            />
          }
        />
      </View>
    )
  }
}
