import {storiesOf} from '@storybook/react-native/src/index';
import ActivityList from './ActivityList';
import React from 'react';
import View from 'react-native-ui-lib/src/components/view/index';

const data = [
  {
    title: 'Title1',
    imageUrl: 'https://cdn.pixabay.com/photo/2016/06/18/17/42/image-1465348_960_720.jpg',
    dollarRating: 4,
    starRating: 3.5,
    id: 0
  },
  {
    title: 'Title2',
    imageUrl: 'https://cdn.pixabay.com/photo/2016/06/18/17/42/image-1465348_960_720.jpg',
    dollarRating: 4,
    starRating: 3.5,
    id: 0
  },
  {
    title: 'Title3',
    imageUrl: 'https://cdn.pixabay.com/photo/2016/06/18/17/42/image-1465348_960_720.jpg',
    dollarRating: 4,
    starRating: 3.5,
    id: 0
  },
  {
    title: 'Title4',
    imageUrl: 'https://cdn.pixabay.com/photo/2016/06/18/17/42/image-1465348_960_720.jpg',
    dollarRating: 4,
    starRating: 3.5,
    id: 0
  },
  {
    title: 'Title5',
    imageUrl: 'https://cdn.pixabay.com/photo/2016/06/18/17/42/image-1465348_960_720.jpg',
    dollarRating: 4,
    starRating: 3.5,
    id: 0
  }
]

storiesOf('Feed')
  .add('ActivityList', () => (
    <View style={{flex: 1}} >
      <ActivityList data={data} />
    </View>
  ))
