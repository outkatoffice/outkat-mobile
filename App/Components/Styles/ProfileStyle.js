import { StyleSheet } from 'react-native'
import colors from '../../Themes/Colors';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  avatarContainer: {
    position: 'absolute',
    bottom: -65,
    alignSelf: 'center',
    borderWidth: 5,
    borderRadius: 1000,
    borderColor: colors.white,
    shadowColor: '#888888',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
  },
  avatar: {
    borderRadius: 20
  },
  headerBanner: {
    flex: 3 / 8,
    backgroundColor: 'rgba(0,0,0,.6)',
    zIndex: 1000
  },
  headerBannerImage: {
    flex: 1,
    opacity: 0.5
  },
  settingsContainer: {
    flex: 5 / 8,
  },
  scrollView: {
    marginLeft: 10,
    marginRight: 10,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    marginTop: 90
  },
  subTitle: {
    textAlign: 'center',
    fontSize: 18,
    color: 'grey',
    marginTop: 10
  },
  sliderContainer: {
    marginBottom: 20,
    marginTop: 20
  },
  slider: {
    marginBottom: 20,
  },
  sliderText: {
    textAlign: 'center',
    fontSize: 20
  },
  sliderElement: {
    flex: 1
  },
  switchContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  switch: {
    marginRight: 20
  },
  switchText: {
    alignSelf: 'center',
    fontSize: 15
  },
  logOutContainer: {
    marginBottom: 30,
  },
  logOutButtonContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  logOutButton: {
    width: 50
  },
  deleteAccountButton: {
    width: 150
  },
  logOutText: {
    textAlign: 'center',
    color: 'grey',
    marginBottom: 30
  }
})
