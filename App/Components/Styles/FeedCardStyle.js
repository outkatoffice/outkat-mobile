import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  lowerArea: {
    flex: 1 / 3,
  },
  titleText: {
    fontSize: 25,
    fontWeight: 'bold'
  },
  halfSplit: {
    flex: 1 / 2,
  },
  horizontalSplitRating: {
    flex: 1 / 2,
    flexDirection: 'row'
  },
  starRating: {
    flex: 1 / 2,
    justifyContent: 'center'
  }
})
