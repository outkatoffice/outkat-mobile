import { StyleSheet } from 'react-native'
import colors from '../../Themes/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  imageContainer: {
    height: 180,
    borderRadius: 8,
    backgroundColor: colors.accent,
    marginBottom: 15
  },
  coverImage: {
    flex: 1,
    borderRadius: 8
  },
  textImageOverlay: {
    position: 'absolute',
    top: 82,
    fontSize: 16,
    color: colors.white,
    alignSelf: 'center'
  },
  geoTextInputContainer: {
    width: '100%',
    backgroundColor: colors.white,
    borderTopColor: colors.white,
    borderBottomColor: colors.accent,
    borderBottomWidth: 1.5
  },
  geoTextInput: {
    fontSize: 18,
    fontWeight: '200',
    color: colors.grey,
    marginLeft: 0,
    paddingLeft: 0
  },
  geoDescription: {
    fontWeight: 'normal'
  },
  predefinedPlaces: {
    color: '#1faadb'
  },
  currentLocationIcon: {
    marginTop: 7
  },
  stepperContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  stepperTextContainer: {
    justifyContent: 'center'
  },
  stepperText: {
    fontSize: 18,
    fontWeight: '200'
  },
  tagsInputStyle: {
    color: colors.accent
  },
  tagsTagStyle: {
    backgroundColor: colors.accent
  }
})
