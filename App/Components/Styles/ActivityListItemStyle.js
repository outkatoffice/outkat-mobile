import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    shadowColor: '#888888',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 2,
    shadowOpacity: 0.8,
    borderRadius: 8
  },
  dollarContainer: {
    flexDirection: 'row'
  },
  title: {
    fontSize: 15,
    fontWeight: '600'
  }
})
