import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 5
  },
  roundButtonContainer: {
    borderWidth: 0,
    borderColor: 'rgba(0,0,0,0.1)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 65,
    height: 65,
    backgroundColor: '#F1636A',
    borderRadius: 100,
    alignSelf: 'center',
    shadowColor: '#888888',
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 5,
    shadowOpacity: 1.0
  },
  buttonIcon: {
    color: '#FFFFFF',
    textAlign: 'center',
    justifyContent: 'center'
  },
  buttonLeft: {
    marginLeft: 30
  },
  buttonRight: {
    marginRight: 30
  },
  cardSection: {
    flex: 13 / 16
  },
  buttonSection: {
    flex: 3 / 16,
    flexDirection: 'row'
  },
  splitThird: {
    flex: 1 / 3
  }
})
