import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import {Dimensions} from 'react-native'
import Swiper from 'react-native-deck-swiper'
import FeedCard from './FeedCard'
import TouchableOpacity from 'react-native-ui-lib/src/components/touchableOpacity/index';
import View from 'react-native-ui-lib/src/components/view/index';
import styles from './Styles/SwipeCardsStyle';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {activityActions, getError, getFeedActivities, hasError, isLoading} from '../modules/activity';
import {createStructuredSelector} from "reselect";
import {connect} from 'react-redux';

const ICON_SIZE = 30

class SwipeCards extends Component {
  swipeLeft () {
    this.refs.swiper.swipeLeft()
  }

  swipeUp () {
    this.refs.swiper.swipeTop()
  }

  swipeRight () {
    this.refs.swiper.swipeRight()
  }

  render () {
    let { height } = Dimensions.get('window')
    height = Math.ceil(height / 1.5)
    return (
      <View style={styles.container}>
        <View style={styles.cardSection}>
          <Swiper
            cardStyle={{height: height, top: 0}}
            renderCard={card =>
              <FeedCard
                title={card.title}
                dollarRating={card.price}
                starRating={card.starRating}
                image={card.picture}
              />
            }
            cards={this.props.cards}
            backgroundColor={'#ffffff'}
            ref='swiper'
            onSwipedLeft={(idx) => this.handleLeftSwipe(idx)}
            onSwipedTop={(idx) => this.handleUpSwipe(idx)}
            onSwipedRight={(idx) => this.handleRightSwipe(idx)}
            onTapCard={(idx) => this.handleOnTap(idx)}
          />
        </View>
        <View style={styles.buttonSection}>
          <View style={styles.splitThird} >
            <TouchableOpacity style={[styles.roundButtonContainer, styles.buttonLeft]} onPress={() => this.swipeLeft()} >
              <MaterialIcons name='clear' size={ICON_SIZE} style={styles.buttonIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.splitThird} >
            <TouchableOpacity style={styles.roundButtonContainer} onPress={() => this.swipeUp()} >
              <MaterialIcons name='star' size={ICON_SIZE} style={styles.buttonIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.splitThird} >
            <TouchableOpacity style={[styles.roundButtonContainer, styles.buttonRight]} onPress={() => this.swipeRight()} >
              <MaterialIcons name='done' size={ICON_SIZE} style={styles.buttonIcon} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  handleLeftSwipe (idx) {
    const activity = this.getActivityByListIndex(idx)
    this.props.setNotInterestedActivity(activity._id)
  }

  handleUpSwipe (idx) {
    const activity = this.getActivityByListIndex(idx)
    this.props.setBeenThereActivity(activity._id)
    this.props.navigation.navigate('Rating', { activity })
  }

  handleRightSwipe (idx) {
    const activity = this.getActivityByListIndex(idx)
    this.props.setInterestedActivity(activity._id)
  }

  getActivityByListIndex (idx) {
    const activities = this.props.activities
    return activities[idx]
  }
  handleOnTap (idx) {
    const activity = this.getActivityByListIndex(idx)
    this.props.navigation.navigate('Details', {activityId: activity._id})
  }
}

const mapStateToProps = createStructuredSelector({
  isLoading: isLoading(),
  hasError: hasError(),
  error: getError(),
  activities: getFeedActivities()
})

const mapDispatchToProps = {
  setInterestedActivity: activityActions.setInterestedActivity,
  setNotInterestedActivity: activityActions.setNotInterestedActivity,
  setBeenThereActivity: activityActions.setBeenThereActivity
}

export default connect(mapStateToProps, mapDispatchToProps)(SwipeCards)

