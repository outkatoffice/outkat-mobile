import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/ActivityListItemStyle'
import ListItem from 'react-native-ui-lib/src/components/listItem/index';
import Image from 'react-native-ui-lib/src/components/image/index';
import {Rating} from 'react-native-ratings';
import DollarRating from './DollarRating';
import {convertToDollarRating} from '../Lib/convertors';

export default class ActivityListItem extends Component {

  handlePress(id) {
    this.props.navigation.navigate('ActivityDetails', {activityId: id})
  }

  render () {
    const item = this.props.activity
    const dollarRating = convertToDollarRating(item.price)
    return (
      <View style={styles.container}>
        <ListItem
          activeOpacity={0.3}
          height={77.5}
          onPress={() => {this.handlePress(item._id)}}
          animation="fadeIn"
          easing="ease-out-expo"
          duration={1000}
          useNativeDriver
          containerStyle={{borderRadius: 8}}>
          <View style={{flex: 1 / 3}}>
            <Image source={{uri: item.picture}} style={{flex: 1, borderRadius: 8}} />
          </View>
          <View style={{flex: 2 / 3, marginLeft: 20, flexDirection: 'column', marginRight: 10}} >
            <View style={{flex: 1 / 2, justifyContent: 'center'}}>
              <Text numberOfLines={1} style={styles.title} >{item.title}</Text>
            </View>
            <View style={{flex: 1 / 2, justifyContent: 'center', flexDirection: 'row'}}>
              <View style={{flex: 1 / 2}}>
                {
                  item.starRating ?
                    <Rating
                    type='star'
                    ratingCount={5}
                    startingValue={item.starRating}
                    imageSize={18}
                    style={styles.starRating}
                    readonly
                  /> :
                  null
                }
              </View>
              <View style={{flex: 1 / 2}}>
                <DollarRating rating={dollarRating} size={20} containerStyle={styles.dollarContainer} />
              </View>
            </View>
          </View>
        </ListItem>
      </View>
    )
  }
}
