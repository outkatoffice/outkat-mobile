import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import {View, Text, TouchableHighlight, ScrollView, Slider, CheckBox, Switch, AsyncStorage} from 'react-native'
import styles from './Styles/ProfileStyle'
import Image from 'react-native-ui-lib/src/components/image/index'
import Avatar from 'react-native-ui-lib/src/components/avatar/index'
import colors from '../Themes/Colors'
import TouchableOpacity from 'react-native-ui-lib/src/components/touchableOpacity/index'
import Button from 'react-native-ui-lib/src/components/button/index'
import HorizontalRule from './HorizontalRule';
import { createStructuredSelector } from "reselect";
import {setLocationFilter, setPriceFilter, coreActions, getLocationFilter, getPriceFilter} from '../modules/core';
import {connect} from 'react-redux';

class Profile extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    const { user, locationFilter, priceFilter } = this.props
    return <View style={styles.container}>
      <View style={styles.headerBanner}>
        <Image source={{uri: user.picture}} style={styles.headerBannerImage} blurRadius={5} />
        <View style={styles.avatarContainer}>
          <Avatar size={130} containerStyle={styles.avatar}
            imageSource={{uri: user.picture}} />
        </View>
      </View>
      <View style={styles.settingsContainer}>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.title}>{`${user.firstName} ${user.lastName}`}</Text>
          {
            user.email ? <Text style={styles.subTitle}>{user.email}</Text> : null
          }
          <View style={styles.sliderContainer}>
            <View style={styles.slider}>
              <Text style={styles.sliderText}>Price: {priceFilter} $</Text>
              <Slider
                style={styles.sliderElement}
                step={1}
                minimumValue={1}
                maximumValue={5}
                value={priceFilter}
                onSlidingComplete={val => this.props.setPriceFilter(val)}
                minimumTrackTintColor={colors.accent}
              />
            </View>
            <View style={styles.slider}>
              <Text style={styles.sliderText}>Radius: {locationFilter} kms</Text>
              <Slider
                style={styles.sliderElement}
                step={1}
                minimumValue={10}
                maximumValue={200}
                value={60}
                onSlidingComplete={val =>
                  this.props.setLocationFilter(val)
                }
                minimumTrackTintColor={colors.accent}
              />
            </View>
          </View>
          <HorizontalRule />
          <View>
            <View style={styles.switchContainer}>
              <Switch
                onValueChange={(val) => console.log(val)}
                value
                onTintColor={colors.accent}
                style={styles.switch}
              />
              <Text style={styles.switchText}>GDPR1</Text>
            </View>
            <View style={styles.switchContainer}>
              <Switch
                onValueChange={(val) => console.log(val)}
                value
                onTintColor={colors.accent}
                style={styles.switch}
              />
              <Text style={styles.switchText}>GDPR2</Text>
            </View>
            <View style={styles.switchContainer}>
              <Switch
                onValueChange={(val) => console.log(val)}
                value
                onTintColor={colors.accent}
                style={styles.switch}
              />
              <Text style={styles.switchText}>GDPR3</Text>
            </View>
          </View>
          <HorizontalRule />
          <View style={styles.logOutContainer}>
            <View style={styles.logOutButtonContainer}>
              <Button label='Log Out' backgroundColor={colors.accent} size='medium' fullWidth={false} style={styles.logOutButton} borderRadius={5} />
            </View>
            <TouchableOpacity>
              <Text style={styles.logOutText}>Privacy Notice</Text>
            </TouchableOpacity>
            <View style={styles.logOutButtonContainer}>
              <Button label='Delete Account' backgroundColor={colors.accent} size='medium' fullWidth={false} style={styles.deleteAccountButton} borderRadius={5} />
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  }
}

const mapStateToProps = createStructuredSelector({
  locationFilter: getLocationFilter(),
  priceFilter: getPriceFilter()
})

const mapDispatchToProps = {
  setLocationFilter: coreActions.setLocationFilter,
  setPriceFilter: coreActions.setPriceFilter
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
