import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import styles from './Styles/FeedCardStyle'
import View from 'react-native-ui-lib/src/components/view/index';
import Text from 'react-native-ui-lib/src/components/text/index';
import Card from 'react-native-ui-lib/src/components/card/index';
import Image from 'react-native-ui-lib/src/components/image/index';
import Rating from 'react-native-ratings/src/rating';
import DollarRating from './DollarRating';

// NEEDED PROPS: title: string, starRating: float, dollarRating: int
export default class FeedCard extends Component {
  render () {
    return (
      <View style={styles.container}>
        <Card shadowType='white30' containerStyle={{flex: 1}}>
          <Image source={{uri: this.props.image}} style={{flex: 1}} />
          <View padding-20 style={styles.lowerArea}>
            <View style={styles.halfSplit}>
              <Text style={styles.titleText}>
                {this.props.title}
              </Text>
            </View>
            <View style={styles.horizontalSplitRating} centerV>
              <Rating
                type='star'
                ratingCount={5}
                startingValue={this.props.starRating}
                imageSize={20}
                style={styles.starRating}
                readonly
              />
              <View style={styles.halfSplit}>
                <DollarRating rating={this.props.dollarRating} />
              </View>
            </View>
          </View>
        </Card>
      </View>
    )
  }
}
